#!/bin/bash

#============================================================
#          FILE:  deploy_splunk-indexer.sh
#         USAGE:  ./deploy_splunk-indexer
#   DESCRIPTION:  Deploys a Google Cloud vm as a splunk indexer
#     ARGUMENTS:  --
#  REQUIREMENTS:  Google Cloud SDK
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  TEAM2
#       COMPANY:  KdG
#       VERSION:  1.0
#       CREATED:  22/10/22
#      REVISION:  ---
#============================================================

# ============= VARIABLES ==============
project_ids=$(gcloud projects list --format="value(projectId)")
options=($project_ids)
len=${#options[@]}

# ============== CHECK FUNCTIONS ==============

function check_user(){
	if   [ "$(id -u)" != "0" ]; then 
		echo "Dit script moet je als root uitvoeren" 1>&2 
		exit 1
	fi
}

function checkGC(){
	which gcloud &>/dev/null
	if [ $? -ne 0 ]
	then
		red "The Google Cloud SDK is not installed."
		echo "installing gcloud sdk..."
		# === Install gcloud sdk ===
		# == Add the Cloud SDK distribution URI as a package source ==
		echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list > /dev/null

		# == Import the Google Cloud Platform public key ==
		curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - > /dev/null

		# == Update the package list and install the Cloud SDK ==
		sudo apt-get update && sudo apt-get install google-cloud-sdk -y > /dev/null


		# == Authorize access using a service account ==
		gcloud auth

		#== Set the project property in the configuration to the property you choose ==
		gcloud init

		[ $? -ne 0 ] && { red "Something went wrong... The Google Cloud SDK is not configured properly."; exit 1; }
	fi
	echo "checking for updates..."
	gcloud components update --quiet &>/dev/null
}

# ============== CHOOSE GCLOUD PROJECT ==============
function choose_project(){
	echo "Choose a project by entering the corresponding number."
	select opt in "${options[@]}"
	do
		if [ $REPLY -le $len -a $REPLY -gt 0 ] 2>/dev/null
		then
			echo "Project set to $opt"
			gcloud config set project $opt
			break
		else
			echo "Please enter a valid number"
		fi
	done
}

# ============== INFRASTRUCTURE ==============
function create_firewall_rules(){
	printf "Creating firewall rules...\n"

	gcloud compute --project=$opt firewall-rules create allow-splunk \
		--description="" \
		--direction=INGRESS \
		--priority=1000 \
		--network=default \
		--action=ALLOW \
		--rules=tcp:8000 \
		--source-ranges=0.0.0.0/0

	gcloud compute --project=$opt firewall-rules create allow-splunk-forwarder \
		--description="" \
		--direction=INGRESS \
		--priority=1000 \
		--network=default \
		--action=ALLOW \
		--rules=tcp:9997 \
		--source-ranges=0.0.0.0/0
}
function create_vm(){
	printf "\nCreating VM..."

	# == Create a static external IP ==
	gcloud compute addresses create splunk-address \
		--global \
		--ip-version IPV4

	# == Create the VM ==
	gcloud beta compute instances create splunk-indexer \
		--project=$opt \
		--zone=europe-west1-b \
		--tags=http-server,https-server,allow-splunk,allow-splunk-forwarder \
		--image-family=ubuntu-2004-lts \
		--image-project=ubuntu-os-cloud \
		--address=splunk-address \
		--metadata-from-file=startup-script=startup.sh
}

# ============== MAIN ==============
check_user
checkGC
choose_project
create_firewall_rules
create_vm