#!/bin/bash

#============================================================
#          FILE:  splunk-forwarder.sh
#         USAGE:  ./splunk-forwarder.sh <indexer ip address>
#   DESCRIPTION:  Configure a splunk forwarder to send data to a splunk indexer
#     ARGUMENTS:  1: IP adress of the splunk  indexer
#  REQUIREMENTS:  Pi-Hole
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  TEAM2
#       COMPANY:  KdG
#       VERSION:  1.0
#       CREATED:  22/10/22
#      REVISION:  ---
#============================================================


# ============== CHECK FUNCTIONS ==============

function check_user(){
	if   [ "$(id -u)" != "0" ]; then
		echo "This script needs to be executed as root." 1>&2
		exit 1
	fi
}

function check_pihole(){
	error_no_pihole="Pi-Hole is not installed. Please install pihole with ../Pi-Hole.sh -install"
	command -v pihole >/dev/null 2>&1 || { echo >&2 $error_no_pihole; exit 1; }
}

function check_argument(){
	if [ $# -lt 1 -o "$1" = "--help" ]; then
		cat <<-END
			Usage: `basename $0` <ip address server>
			Example: `basename $0` 35.205.23.229
		END
		exit 1
	fi
}

# ============== INSTALLATIONS ==============

function install_snort(){
	sudo apt-get update && sudo apt-get upgrade -y
	sudo apt-get install snort -y
}

function install_splunkforwarder(){
	# === Download the correct Universal forwarder ===
	wget -O splunkforwarder-8.1.5-9c0c082e4596-Linux-arm.tgz "https://download.splunk.com/products/universalforwarder/releases/8.1.5/linux/splunkforwarder-8.1.5-9c0c082e4596-Linux-arm.tgz"

	# === Extract the downloaded folder in /opt ===
	tar xvzf splunkforwarder-8.1.5-9c0c082e4596-Linux-arm.tgz -C /opt

	# === Start the splunk forwarder ===
	sudo /opt/splunkforwarder/bin/splunk start
}

# ============== CONFIGURATION ==============
function configure_outputs(){
	# === Configure to which indexer the data needs to be send ===
	echo "[tcpout]
defaultGroup = my_indexer

[tcpout:my_indexer]
server=${1}:9997

[tcpout-server://${1}:9997]" > /opt/splunkforwarder/etc/system/local/outputs.conf
}

function configure_inputs(){
	# === monitor the pihole log files to send data to the splunk indexer ===
	sudo /opt/splunkforwarder/bin/splunk add monitor /var/log/pihole/* -index dns -sourcetype pi

	# === monitor the snort log files to send data to the splunk indexer ===
	sudo /opt/splunkforwarder/bin/splunk add monitor /var/log/snort/* -index ids -sourcetype snort_alert_full

	# === restart the splunk server ===
	sudo /op/splunkforwarder/bin/splunk restart
}

# ============== MAIN ==============
check_user
check_pihole
check_argument
install_snort
install_splunkforwarder
configure_outputs $1
configure_inputs
