<br />
<div align="center">
    <img src="https://community.splunk.com/legacyfs/online/avatars/504187.jpg" alt="" width="80" height="80">
  </a>

<h3 align="center">Integratieproject 2 - ISB: TEAM 2</h3>

  <p align="center">
  2022-2023
  </p>
</div>


<details>
	<summary>Team members</summary>
	  <ol>
	  	<li>Walid Loutfi</a></li>
		<li>Viktor Koeklenberg</a></li>
		<li>Thomas Van Rampelberg</a></li>
		<li>Nick Van Wijnsberghe</a></li>
	  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About This Project

<img src="https://styles.redditmedia.com/t5_3rih2q/styles/communityIcon_goek54psbye61.png?width=256&s=9b63e8ddd542297b26ce3214fab1453ec3c9fbf8" alt="" width="100" height="100">

As an extension on our Pi-Hole project, we have made a custome Home Network Monitor dashboard in Splunk using Pi-Hole for DNS traffic and Snort as an ID (Intrusion Detection System).
We have configured the Raspberry Pi as the Splunk Forwarder that sends data to a Splunk Indexer on the Google Cloud. 

### Built With

* [Bash](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html)

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.


### Prerequisites
* A Raspberry Pi 4 

* A Debian or Ubuntu based Linux distribution

* Git comes installed by default on most Debian machines! You can check this with the following command.
  ```sh
  git --version
  ```
  If the output shows a Git version, you already have Git installed on your Raspberry Pi. 
  If not, run the following command to install Git on your machine.
  ```sh
  sudo apt-get update && sudo apt-get install git
  ```
  
* Google Cloud SDK
  ```sh
  #Add the Cloud SDK distribution URI as a package source
  echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list > /dev/null

  #Import the Google Cloud Platform public key
  curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add - > /dev/null

  #Update the package list and install the Cloud SDK
  sudo apt-get update && sudo apt-get install google-cloud-sdk -y > /dev/null
  
  #Authorize access using a service account
  gcloud auth
  
  #Set the project property in the configuration to the property you choose
  gcloud init
  ```

* A billing account linked to your Google Cloud Project.

* A Splunk Enterprise Account

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/walidltf/pi-hole.git
   ```
2. Navigate to the 'Splunk' directory and give execute writes to these 2 scripts
   ```sh
   cd pi-hole/Splunk
   chmod u+x deploy_splunk-indexer.sh && chmod u+x splunk-forwarder.sh
   ```
You only need to run deploy_splunk-indexer.sh on a Linux machine and splunk-forwarder.sh on the Raspberry Pi. 
startup.sh is being executed within deploy_splunk-indexer.sh.

<!-- USAGE -->

## Usage

This section will show useful examples of how this project can be used. 
This project automates the Splunk application installation and configuration process for quicker provisioning.

* On a Debian based linux machine you can deploy a GCP VM configured as a Splunk indexer.
	```sh
   ./deploy_splunk-indexer.sh
   ```
* The Raspberry Pi will be configured as a Splunk Forwarder.
	```sh
   ./splunk-forwarder.sh <external ip address GC VM>
   ```
   
After running these scripts, you can make your own custom dashboard with logdata send from you Raspberry Pi.