#!/bin/bash

function install_splunkindexer(){
	# === Download the correct Splunk indexer ===
	sudo wget -O splunk-8.2.3-cd0848707637-Linux-x86_64.tgz "https://download.splunk.com/products/splunk/releases/8.2.3/linux/splunk-8.2.3-cd0848707637-Linux-x86_64.tgz"

	# === Extract the downloaded folder in /opt ===
	tar xvzf splunk-8.2.3-cd0848707637-Linux-x86_64.tgz -C /opt

	# === Start the splunk indexer ===
	sudo /opt/splunk/bin/splunk start
}

function create_indexes(){
	# === Create an index for Pi-hole ===
	/opt/splunk/bin/splunk add index dns

	# === Create an index for Snort ===
	/opt/splunk/bin/splunk add index ids
}

function configure_receiving_port(){
	echo -e "[splunktcp://9997]\ndisabled = 0" > /opt/splunk/etc/system/local/inputs.conf
}

install_splunkindexer
create_indexes
configure_receiving_port
