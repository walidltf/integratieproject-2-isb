<br />
<div align="center">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/archive/0/00/20180808204859%21Pi-hole_Logo.png/120px-Pi-hole_Logo.png" alt="" width="80" height="80">
  </a>

<h3 align="center">Integratieproject 2 - ISB: TEAM 2</h3>

  <p align="center">
  2022-2023
  </p>
</div>


<details>
	<summary>Team members</summary>
	  <ol>
	  	<li>Walid Loutfi</a></li>
		<li>Viktor Koeklenberg</a></li>
		<li>Thomas Van Rampelberg</a></li>
		<li>Nick Van Wijnsberghe</a></li>
	  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About This Project

<img src="https://styles.redditmedia.com/t5_3rih2q/styles/communityIcon_goek54psbye61.png?width=256&s=9b63e8ddd542297b26ce3214fab1453ec3c9fbf8" alt="" width="100" height="100">

Pi-hole is a Linux network-level advertisement and Internet tracker blocking application which acts as a DNS sinkhole and optionally a DHCP server, 
intended for use on a private network. It is designed for low-power embedded devices with network capability, such as the Raspberry Pi.

Pi-hole has the ability to block traditional website advertisements as well as advertisements in unconventional places, such as smart TVs and mobile operating system advertisements.

### Built With

* [Bash](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html)

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.


### Prerequisites
* A Raspberry Pi 4 

* Git comes installed by default on most Debian machines! You can check this with the following command.
  ```sh
  git --version
  ```
  If the output shows a Git version, you already have Git installed on your Raspberry Pi. 
  If not, run the following command to install Git on your machine.
  ```sh
  sudo apt-get update && sudo apt-get install git
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/walidltf/pi-hole.git
   ```
2. Navigate to the 'pi-hole' directory and give execute writes to Pi-Hole.sh
   ```sh
   cd pi-hole
   chmod u+x pi-Hole.sh
   ```

<!-- USAGE -->

## Usage

This section will show useful examples of how this project can be used. 
This project gives an easy way to install and manage Pi-Hole.

* The installation process of Pi-Hole happens all automatically when running the following command.
	```sh
   ./Pi-Hole.sh -install
   ```
* You can add and remove domains from your black/whitelist.
	```sh
   ./Pi-Hole.sh -w add
   ./Pi-Hole.sh -b remove
   ```
* To see more examples of the possible options and arguments that can be given to the script. Run the following command.
	```sh
   ./Pi-Hole.sh -h
   ```
   OR
   	```sh
   ./Pi-Hole.sh --help
   ```