#!/bin/bash

#============================================================
#          FILE:  Pi-Hole.sh
#         USAGE:  ./Pi-Hole.sh -h
#   DESCRIPTION: Manage your network wide adblocker Pi-Hole
#
#     ARGUMENTS: -install: installs Pi-Hole on your system
#                -uninstall: uninstalls Pi-Hole from your system:
#                -w: manages your whitelist
#                -b: manages your blacklist
#
#	OPTIONS: add: adds domains to your whitelist or blacklist
#                remove: removes domains from your whitelist or blacklist
#
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  TEAM2
#       COMPANY:  KdG
#       VERSION:  1.0
#       CREATED:  20/10/22
#      REVISION:  ---
#============================================================

# ============== VARIABLES ==============
ARG=$1
OPT=$2

# ============== CHECK FUNCTIONS ==============
function check_user(){
	if   [ "$(id -u)" != "0" ]; then
		echo "This script needs to be executed as root" 1>&2
		exit 1
	fi
}

function check_pihole(){
	error_no_pihole="Pi-Hole is not installed."
	if [ command -v pihole >/dev/null 2>&1 ]; then
		read -p "$error_no_pihole Would you like to install Pi-Hole? (y/n): " answer
		if [ $answer = "y" ] || [ $answer = "Y" ]; then
			install
		fi
	fi
}

# ============== COLOR FUNCTIONS ==============
function green(){
	echo -en "\e[32m$1 \e[39m"
}

function red(){
	echo -e "\e[31m$1 \e[39m"
}

# ============== HELP FUNCTION ==============
function help(){
	echo "Usage: `basename $0` [ARGUMENT] [OPTION]
Description: Manages Network-Wide Ad blocker Pi-Hole

ARGUMENTS: 	-install: installs Pi-Hole on your system
		-uninstall: uninstalls Pi-Hole from your system:
		-w: manages your whitelist
		-b: manages your blacklist

OPTIONS:	add: adds domains to your whitelist or blacklist
		remove: removes domains from your whitelist or blacklist

EXAMPLES:	./Pi-Hole.sh -w add
		./Pi-Hole.sh -b remove
		./Pi-Hole.sh -install
		./Pi-Hole.sh -uninstall
"
}

# ============== PI-HOLE INSTALLATION ==============

function list_interfaces(){
	interfaces=$(route -ne | tail -n+3 | tr -s " " | cut -d " " -f 8 | sort | uniq)

	count=0
	for interface in $interfaces
        do
		count=$((count+1))
                echo "${count}) $interface"
        done

        read -p "Interface: " newinter

        found=0
        for  interface in $interfaces
	do
		if [ $newinter == $interface ]
                then
			found=1
                fi
	done

	[ $found -eq "0" ] && { red "The interface you entered is not available.\n"; exit 1; }

}

function write_static_ip(){
	dgw=$(route -ne | tail -n+3 | tr -s " " | cut -d " " -f 2 | grep -v "^0" | sort | uniq)
        subnet=$(route -ne | tail -n 1 | tr -s " " | cut -d " " -f 3)
        dns=$(cat /etc/resolv.conf | grep nameserver | cut -d " " -f 2 | tr "\n" " ")

	read -p "Static IP (make sure the static ip is in the range of your subnet in the form of 0.0.0.0/0 (subnet=${subnet} default gateway=${dgw}): " ip

	echo -e "interface ${interface}\nstatic ip_address=${ip}\nstatic routers=${dgw}\nstatic domain_name_servers=${dns}" >> /etc/dhcpcd.conf

}

function configure_static_ip() {
	currint=$(cat /etc/dhcpcd.conf | grep -e '^interface eth[0-9]*' | cut -d '=' -f2)

	if [ ! -z "$currint" ]; then
		read -p "$currint is configured as a static ip. Do You want to use this interface for Pi-Hole? (y/n): " answer
                if [ $answer = "y" ] || [ $answer = "Y" ]; then
                        write_static_ip
                fi
	else
		echo "On which interface do you want to configure a static IP?"
                echo "Available interfaces: "
                list_interfaces
                write_static_ip
	fi
}

function install() {
	configure_static_ip
	curl -sSL https://install.pi-hole.net | bash
}

# ============== PI HOLE MANAGEMENT ==============

function add_to_blacklist() {
	read -p "Do you want to add a regex or domain name to the blacklist(r/d): " answer

	case $answer in
		"d")
			read -p "Enter domain name(s): "  domains

			IFS=" "
			for domain in $domains
			do
				host $domain 2>&1 >/dev/null

				if [ $? -eq 0 ]; then
					pihole -b $domain
					green "The domain $domain is successfully added to your blacklist.\n"
				else
					red "Domain $domain is not valid.\n"
				fi
			done
			unset IFS
			if [ -z $domains ]; then
				red "You must enter a domain.\n"
				exit 1
			fi
			;;
		"r")
			read -p "Enter a regex for your domain name: " regex
			pihole --regex $regex
			green "The regex $regex is successfully added to your blacklist.\n"
			;;
		*)
			red "The option $answer is not valid\n"
			exit 1
			;;
	esac

}

function remove_from_blacklist() {
	read -p "Enter the domain name(s) you want to remove from the blacklist: " domains

	IFS=" "
        for domain in $domains
	do
		host $domain 2>&1 >/dev/null

		if [ $? -eq 0 ]; then
			pihole -b -d $domain
			green "The domain $domain is successfully added to your blacklist.\n"
		else
			red "Domain $domain is not valid.\n"
		fi
	done
        unset IFS

	if [-z $domains ]; then
		red "You must enter a domain!\n"
		exit 1
	fi

}

function add_to_whitelist() {
	read -p "Do you want to add a regex or domain name to your whitelist(r/d): " answer

        case $answer in
                "d")
                        read -p "Enter domain name(s): "  domains

			IFS=" "
			for domain in $domains
			do
				host $domain 2>&1 >/dev/null

                        	if [ $? -eq 0 ]; then
                                	pihole -w $domain
					green "The domain $domain is successfully added to your whitelist.\n"
                        	else
                                	red "Domain $domain is not valid.\n"
                        	fi
			done
			unset IFS

			if [ -z $domains ]; then
				red "You must enter a domain name.\n"
				exit 1
			fi
                        ;;
                "r")
                        read -p "Enter a regex for your domain name: " regex
                        pihole --white-regex $regex
			green "The regex $regex is successfully added to your whitelist.\n"
                        ;;
                *)
                        red "The option $answer is not valid.\n"
                        exit 1
                        ;;
        esac

}

function remove_from_whitelist() {
	read -p "Enter the domain(s) to delete from your whitelist: " domains

	IFS=" "
	for domain in $domains
	do
		host $domain 2>&1 >/dev/null

		if [ $? -eq 0 ]; then
			pihole -w -d $domain
			green "Domain $domain is successfully removed from your whitelists.\n"

       	 	elif [ -z "$domain" ]; then
			red "You must enter a domain!\n"

       		else
			red "Domain $domain is not valid.\n"
        	fi
	done
	unset IFS
}

function uninstall(){
	read -p "Are you sure you want to uninstall Pi-Hole? (y/n): " answer
	if [ $answer = "y" ] || [ $answer = "Y" ]; then
		pihole uninstall
	else
		exit 1
	fi
}

# ============================================== MAIN ============================================== 
check_user
check_pihole
case $ARG in
	-install)
		install
		;;
	-b)
		case $OPT in
			add)
				add_to_blacklist
				;;
			remove)
				remove_from_blacklist
				;;
			*)
				red "Wrong Argument(s). Please try again."
				help
				;;
		esac
		;;
	-w)
		case $OPT in
			add)
				add_to_whitelist
				;;
			remove)
				remove_from_whitelist
				;;
			*)
				red "Wrong Argument(s). Please try again."
				help
				;;
		esac
		;;
	-uninstall)
		uninstall
		;;
	-h | --help)
		help
		;;
	*)
		red "Wrong Argument(s). Please Try again."
		help
		;;
esac
